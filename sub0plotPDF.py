# -*- coding: utf-8 -*-
"""
Created on Thu Jan 12 21:36:17 2017

@author: celin
"""

#%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt
from collections import deque
import pandas as pd


class Horse:
    def __init__(self,id):
        self.weight = max(0, np.random.normal(5,2,1)[0])
        self.name = 'horse_' + str(id)

class Ball:
    def __init__(self,id):
        self.weight = max(0, 1 + np.random.normal(1,0.3,1)[0])
        self.name = 'ball_' + str(id)

class Bike:
    def __init__(self,id):
        self.weight = max(0, np.random.normal(20,10,1)[0])
        self.name = 'bike_' + str(id)

class Train:
    def __init__(self,id):
        self.weight = max(0, np.random.normal(10,5,1)[0])
        self.name = 'train_' + str(id)
        
class Coal:
    def __init__(self,id):
        self.weight = 47 * np.random.beta(0.5,0.5,1)[0]
        self.name = 'coal_' + str(id)
        
class Book:
    def __init__(self,id):
        self.weight = np.random.chisquare(2,1)[0]
        self.name = "book_" + str(id)
        
class Doll:
    def __init__(self,id):
        self.weight = np.random.gamma(5,1,1)[0]
        self.name = "doll_" + str(id)

class Block:
    def __init__(self,id):
        self.weight = np.random.triangular(5,10,20,1)[0]
        self.name = "blocks_" + str(id)
        
class Gloves:
    def __init__(self,id):
        self.weight = 3.0 + np.random.rand(1)[0] if np.random.rand(1) < 0.3 else np.random.rand(1)[0]
        self.name = "gloves_" + str(id)

def plot_gift(g,i, bins=100):
    wvec = [x.weight for x in eval(g)]
    plt.figure(i)
    plt.suptitle(g + " = " + str(sum(wvec)))
    plt.hist(wvec, bins=bins, normed=True, stacked=True)
    
class Gift:
    def __init__(self, name, ngifts=1, nsamples=1):
        self.name = name
        if name=='horse':
            self.weight = np.maximum(0, np.random.normal(5,2,(nsamples, ngifts))).sum(axis=1)
        elif name=='ball':
            self.weight = np.maximum(0, 1 + np.random.normal(1,0.3,(nsamples, ngifts))).sum(axis=1)
        elif name=='bike':
            self.weight = np.maximum(0, np.random.normal(20,10,(nsamples, ngifts))).sum(axis=1)
        elif name=='train':
            self.weight = np.maximum(0, np.random.normal(10,5,(nsamples, ngifts))).sum(axis=1)
        elif name=='coal':
            self.weight = 47 * np.random.beta(0.5,0.5,(nsamples, ngifts)).sum(axis=1)
        elif name=='book':
            self.weight = np.random.chisquare(2,(nsamples, ngifts)).sum(axis=1)
        elif name=='doll':
            self.weight = np.random.gamma(5,1,(nsamples, ngifts)).sum(axis=1)
        elif name=='block':
            self.weight = np.random.triangular(5,10,20,(nsamples, ngifts)).sum(axis=1)
        elif name=='gloves':
            gloves1 = 3.0 + np.random.rand(nsamples, ngifts)
            gloves2 = np.random.rand(nsamples, ngifts)
            gloves3 = np.random.rand(nsamples, ngifts)
            self.weight = np.where(gloves2 < 0.3, gloves1, gloves3).sum(axis=1)
    
class Bag:
    def __init__(self, gift_dict, nsamples=1):
        gifts = [Gift(g, gift_dict[g], nsamples) for g in gift_dict.keys()]
        weights = np.array([g.weight for g in gifts])
        self.weight = weights.sum(axis=0)
        self.weight = np.where(self.weight <= 50, self.weight, 0.0)
        self.mean = self.weight.mean()
        self.std = self.weight.std()

    def plot_bag(self, i, bins, normed=True, stacked=True):
        plt.figure(i)
        plt.suptitle("Average = " + str(self.mean) + ", Std = " + str(self.std))
        self.hist = plt.hist(self.weight, bins=bins, normed=normed, stacked=stacked)
        self.hist = self.hist[:2]
        self.prob_zero = self.hist[0][0]
        
class MetaBag(Bag):
    def __init__(self, bag_choices, bag_probs, nsamples=1):
        """Linearly combine bags with weights
        """
        # gift_dict as input to Bag
        self.bag_choices = bag_choices
        # probabilities of each bag
        self.bag_probs = bag_probs
        self.nbags = len(self.bag_choices)
        self.nsamples = nsamples
        self.bags = [Bag(bag_choices[i], int(nsamples*bag_probs[i]))
            for i in range(self.nbags)]
        self.weight = np.concatenate([self.bags[i].weight for i in range(self.nbags)])
        self.mean = self.weight.mean()
        self.std = self.weight.std()
        

if __name__=='__main__':
    np.random.seed(2017)
#    amt = 100000
#    
##    book = 
#    
#    gift_dict1 = {}
#    gift_dict1['book'] = 0
#    gift_dict1['coal'] = 1
#    gift_dict1['doll'] = 0
#    gift_dict1['horse'] = 0
#    gift_dict1['ball'] = 1
#    gift_dict1['gloves'] = 1
#    bag = Bag(gift_dict1, amt)
#    bag.plot_bag(0, bins=100)
#    print(bag.mean)
#    print(bag.std)
#    print(bag.prob_zero)
#    
#    gift_dict2 = {}
#    gift_dict2['book'] = 0
#    gift_dict2['coal'] = 1
#    gift_dict2['doll'] = 0
#    gift_dict2['horse'] = 0
#    gift_dict2['ball'] = 2
#    gift_dict2['gloves'] = 0
#    
#    gift_dict3 = {}
#    gift_dict3['book'] = 0
#    gift_dict3['coal'] = 1
#    gift_dict3['doll'] = 0
#    gift_dict3['horse'] = 0
#    gift_dict3['ball'] = 0
#    gift_dict3['gloves'] = 2
#    
#    bag_choices = [gift_dict1, gift_dict2, gift_dict3]
#    bag_probs = [0.1, 0.2, 0.7]
#    meta_bag = MetaBag(bag_choices, bag_probs, nsamples=20000)
#    meta_bag.plot_bag(1, bins=100)

    bags = gen_bags()
    nbags = len(bags)    
    bags = pd.DataFrame(columns=gift_types+['mean', 'std'], 
                        data=bags)    
    bags['var'] = bags['std']**2
    bags = bags[bags[gift_types].sum(axis=1) >= 3].reset_index(drop=True)    
    bags.head()