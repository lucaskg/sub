# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 20:12:32 2017

@author: celin
"""

from gurobipy import *
import pandas as pd
import numpy as np

if 'bags' not in locals():
    bags = pd.read_csv('bags.csv')

nbags, _ = bags.shape
#nbags = 20000
probs = bags.iloc[:, 11:].as_matrix()
bin_scores = np.arange(0,50)
probs = probs*bin_scores
mask = np.array([1 for i in range(50)])
mask[:15] = 0
mask[15:30] = 1
mask[30:45] = 1
mask[45:] = 0
obj_vec = np.sum(probs*mask.T, axis=1)
#obj_vec = bags['mean'].as_matrix()
con_mtx = bags.iloc[:, :9].as_matrix()

gifts = pd.read_csv('gifts.csv').values
gifts = [str(gifts[i]) for i in range(gifts.shape[0])]
gifts = [gifts[i].rstrip(']').lstrip('[').rstrip("'").lstrip("'").split('_')[0] for i in range(len(gifts))]
cnt = {}
for g in gifts:
    if g in cnt.keys():
        cnt[g] += 1
    else:
        cnt[g] = 1

cnt_gift = np.array([cnt[i] for i in bags.columns.tolist()[:9]])

m = Model('sub')

x = m.addVars(nbags, vtype=GRB.INTEGER, obj=obj_vec)

m.setAttr('ModelSense', -1)

m.update()

#for j in range(9):
#    m.addConstr(quicksum(con_mtx[i,j]*x[i] for i in range(nbags))<=cnt_gift[j])
    
m.addConstrs((quicksum(con_mtx[i,j]*x[i] for i in range(nbags))<=cnt_gift[j] for j in range(9)), 'c')
m.addConstr(quicksum(x[i] for i in range(nbags))<=1000)

m.optimize()

x_v = {}
for i in range(nbags):
    if x[i].x > 0.5:
        x_v[i] = x[i].x